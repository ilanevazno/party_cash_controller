import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:party_cash_controller/ioc.dart';
import 'package:party_cash_controller/services/routes.dart';
import 'package:party_cash_controller/stores/auth_store/auth_store.dart';
import 'package:party_cash_controller/stores/user_store/user_store.dart';
import 'package:party_cash_controller/utils/flushbar.dart';
import 'package:pinput/pin_put/pin_put.dart';

class OtpPage extends StatefulWidget {
  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  final AuthStore _authStore = ioc.get<AuthStore>();
  final UserStore _userStore = ioc.get<UserStore>();

  final _pinPutFocusNode = FocusNode();
  final _pinPutController = TextEditingController();

  @override
  void dispose() {
    _pinPutController.dispose();
    _pinPutFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    // TODO: loading state in UI

    final _pinPutDecoration = BoxDecoration(
      color: const Color.fromRGBO(128, 128, 128, 1),
      borderRadius: BorderRadius.circular(5.0),
    );

    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(30.0),
          child: PinPut(
            eachFieldWidth: 35.0,
            autofocus: true,
            eachFieldHeight: 35.0,
            withCursor: true,
            fieldsCount: 6,
            disabledDecoration: const BoxDecoration(
              color: Color.fromRGBO(255, 0, 0, 1),
            ),
            focusNode: _pinPutFocusNode,
            controller: _pinPutController,
            onSubmit: (String pin) => _verifyPin(pin),
            submittedFieldDecoration: _pinPutDecoration,
            selectedFieldDecoration: _pinPutDecoration,
            followingFieldDecoration: _pinPutDecoration,
            pinAnimationType: PinAnimationType.scale,
            textStyle: theme.textTheme.bodyText1,
          ),
        ),
      ),
    );
  }

  void _verifyPin(String pin) async {
    try {
      await _authStore.verifyPin(pin);

      final navigator = Navigator.of(context);

      if (await _userStore.isNewUser) {
        navigator.pushReplacementNamed(AppRoutes.register);
      } else {
        await _userStore.loadUser();

        navigator.pushReplacementNamed(AppRoutes.main);
      }
    } catch (err) {
      _showSnackBar(context);
      _pinPutController.text = '';
      _pinPutFocusNode.unfocus();
      print('Pin verification error: $err');
    }
  }

  void _showSnackBar(BuildContext context) {
    showFlushbar(
      context,
      message: 'Неверный пин, попробуйте снова',
      type: FlushbarType.error,
    );
  }
}
