import 'package:flutter/material.dart';
import 'package:party_cash_controller/features/widgets/fade_in_from_network/fade_in.dart';
import 'package:party_cash_controller/ioc.dart';
import 'package:party_cash_controller/models/user_model/user_model.dart';
import 'package:party_cash_controller/services/routes.dart';
import 'package:party_cash_controller/stores/auth_store/auth_store.dart';
import 'package:party_cash_controller/stores/user_store/user_store.dart';
import 'package:party_cash_controller/utils/flushbar.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final UserModel? user = ioc.get<UserStore>().user;
    final NavigatorState navigator = Navigator.of(context);

    return Drawer(
      child: ListTileTheme(
        minLeadingWidth: 20,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: theme.colorScheme.primary,
              ),
              child: Row(
                children: [
                  Container(
                    width: 100,
                    height: 100,
                    decoration: const BoxDecoration(shape: BoxShape.circle),
                    child: CircleAvatar(
                      backgroundColor: theme.colorScheme.surface,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50.0),
                        child: FadeIn(
                          imgUrl: user?.photoUrl ?? '',
                          errorBuilder: (context, exception, stacktrace) {
                            print(exception);
                            print(stacktrace);
                            return Icon(
                              Icons.person,
                              color: theme.colorScheme.onSurface,
                              size: 50.0,
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 10.0),
                  if (user != null)
                    Container(
                      width: 160.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            user.fullname,
                            style: theme.primaryTextTheme.headline6,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                          const SizedBox(height: 5),
                          Text(user.phone, style: theme.primaryTextTheme.subtitle1),
                        ],
                      ),
                    ),
                ],
              ),
            ),
            ListTile(
              leading: const Icon(Icons.person),
              title: const Text('Профиль'),
              onTap: () {
                navigator.pop();
              },
            ),
            ListTile(
              leading: const Icon(Icons.people_alt),
              title: const Text('Друзья'),
              onTap: () {
                navigator.pop();
                navigator.pushNamed(AppRoutes.friends);
              },
            ),
            ListTile(
              leading: const Icon(Icons.settings),
              title: const Text('Настройки'),
              onTap: () {
                navigator.pop();
              },
            ),
            const Divider(),
            ListTile(
              leading: const Icon(Icons.exit_to_app_outlined),
              title: const Text('Выйти'),
              onTap: () => signOut(context),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> signOut(BuildContext context) async {
    final AuthStore authStore = ioc.get<AuthStore>();

    try {
      await authStore.signOut();

      Navigator.of(context).pushReplacementNamed(AppRoutes.auth);
    } catch (e) {
      showFlushbar(
        context,
        message: 'Не получается выйти\nПожалуйста, попробуйте позже',
        type: FlushbarType.error,
      );
    }
  }
}
