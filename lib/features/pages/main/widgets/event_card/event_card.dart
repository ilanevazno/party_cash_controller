import 'package:flutter/material.dart';
import 'package:party_cash_controller/features/widgets/fade_in_from_network/fade_in.dart';
import 'package:party_cash_controller/models/base_event_model/base_event_model.dart';

class EventCard extends StatelessWidget {
  final String title;
  final EventStatus status;
  final void Function() onTap;
  final String? imgUrl;

  EventCard({
    required this.title,
    required this.status,
    required this.onTap,
    this.imgUrl,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final imgUrl = this.imgUrl;

    final Widget eventIcon = Icon(
      Icons.event,
      color: theme.colorScheme.onSurface,
      size: 30.0,
    );

    return Card(
      child: InkWell(
        onTap: onTap,
        child: ListTile(
          leading: Container(
            width: 95.0,
            height: 95.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              color: theme.colorScheme.surface,
            ),
            child: imgUrl != null
                ? ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: FadeIn(
                      imgUrl: imgUrl,
                      errorBuilder: (context, exception, stacktrace) {
                        return eventIcon;
                      },
                    ),
                  )
                : eventIcon,
          ),
          title: Text(
            title,
            overflow: TextOverflow.fade,
            maxLines: 1,
            softWrap: false,
          ),
          subtitle: status == EventStatus.active
              ? Text('В процессе', style: TextStyle(color: theme.colorScheme.primary))
              : const Text('Завершено'),
        ),
      ),
    );
  }
}
