import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:party_cash_controller/features/pages/main/widgets/event_card/event_card.dart';
import 'package:party_cash_controller/features/pages/main/widgets/main_drawer/main_drawer.dart';
import 'package:party_cash_controller/models/event_model/event_model.dart';
import 'package:party_cash_controller/models/base_event_model/base_event_model.dart';
import 'package:party_cash_controller/models/full_event_model/full_event_model.dart';
import 'package:party_cash_controller/services/routes.dart';
import 'package:party_cash_controller/stores/events_store/events_store.dart';
import 'package:party_cash_controller/ioc.dart';
import 'package:party_cash_controller/utils/flushbar.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final EventsStore _eventsStore = ioc.get<EventsStore>();

  @override
  void initState() {
    _eventsStore.loadNotArchivedEvents();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Observer(
      builder: (_) => DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Главное меню'),
            bottom: const TabBar(
              tabs: [
                Tab(text: 'Мои события'),
                Tab(text: 'Группы'),
              ],
            ),
          ),
          drawer: MainDrawer(),
          body: TabBarView(
            children: [
              Column(
                children: [
                  Expanded(
                    child: _eventsStore.loading
                        ? SpinKitSquareCircle(color: theme.colorScheme.secondary, size: 70)
                        : ListView(children: buildEventsList(_eventsStore.eventList)),
                  ),
                ],
              ),
              const Center(
                child: Text('TODO'),
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton.extended(
            onPressed: () => Navigator.of(context).pushNamed(AppRoutes.createEvent),
            icon: const Icon(Icons.add),
            label: const Text('Новое событие'),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        ),
      ),
    );
  }

  List<Widget> buildEventsList(List<BaseEventModel> events) {
    return events
        .map(
          (BaseEventModel baseEvent) => EventCard(
            title: baseEvent.name,
            status: baseEvent.status,
            onTap: () => _handleEventClick(baseEvent),
            imgUrl: baseEvent.imgUrl,
          ),
        )
        .toList();
  }

  Future<void> _handleEventClick(BaseEventModel baseEvent) async {
    // TODO: Loading
    try {
      final FullEventModel fullEvent = await _eventsStore.getFullEvent(baseEvent);

      Navigator.of(context).pushNamed(AppRoutes.event, arguments: fullEvent);
    } catch (e) {
      print('Event card click error: $e');
      showFlushbar(
        context,
        message: 'Не удалось загрузить детали события.\nПожалуйста, попробуйте позже',
        type: FlushbarType.error,
      );
    }
  }
}
