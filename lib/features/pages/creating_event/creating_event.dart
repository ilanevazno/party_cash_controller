import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:party_cash_controller/features/widgets/input_field/input_field.dart';
import 'package:party_cash_controller/features/widgets/main_button/main_button.dart';
import 'package:party_cash_controller/features/widgets/photo_upload/photo_upload.dart';
import 'package:party_cash_controller/ioc.dart';
import 'package:party_cash_controller/stores/events_store/events_store.dart';
import 'package:party_cash_controller/stores/user_store/user_store.dart';
import 'package:party_cash_controller/utils/flushbar.dart';

class CreatingEventPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CreatingEventPageState();
  }
}

class _CreatingEventPageState extends State<CreatingEventPage> {
  final TextEditingController eventNameController = TextEditingController();
  File? image;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Создание события'),
        ),
        body: Column(
          children: [
            PhotoUpload(
              selectedImage: image,
              onPickedImage: _setSelectedPhoto,
            ),
            Container(
              child: Column(
                children: [
                  InputField(placeholder: 'Название события', controller: eventNameController),
                  Container(
                      child: MainButton(
                          title: 'Создать событие',
                          onPressed: () {
                            _onCreatingEvent(context);
                          }),
                      margin: const EdgeInsets.only(top: 15.0))
                ],
              ),
              width: 300.0,
              margin: const EdgeInsets.only(top: 15.0),
            ),
          ],
        ));
  }

  void _onCreatingEvent(BuildContext context) async {
    if (eventNameController.text != '' && eventNameController.text.length < 45) {
      EventsStore eventsStore = ioc.get<EventsStore>();
      UserStore userStore = ioc.get<UserStore>();

      Map<String, dynamic>? createdEvent =
          await eventsStore.createEvent(name: eventNameController.text, image: image);

      if (createdEvent != null) {
        userStore.addEventToUser(createdEvent['id']);

        Navigator.of(context, rootNavigator: true).pop();
      } else {
        showFlushbar(
          context,
          type: FlushbarType.error,
          message: 'Произошла ошибка при попытке создания события',
        );
      }
    }
  }

  void _setSelectedPhoto(File filePath) {
    setState(() {
      image = filePath;
    });
  }
}
