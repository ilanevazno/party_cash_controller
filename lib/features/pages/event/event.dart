import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:party_cash_controller/features/pages/event/widgets/event_image/event_image.dart';
import 'package:party_cash_controller/models/base_event_model/base_event_model.dart';
import 'package:party_cash_controller/models/full_event_model/full_event_model.dart';

class EventPage extends StatelessWidget {
  final FullEventModel _event;

  EventPage(this._event);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final String? imgUrl = _event.imgUrl;

    return Observer(
      builder: (_) => Scaffold(
        appBar: AppBar(
          title: const Text(
            'Детали события',
            overflow: TextOverflow.fade,
          ),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.settings),
            )
          ],
        ),
        body: Column(
          children: [
            Row(
              children: [
                if (imgUrl != null)
                  Expanded(
                    child: EventImage(imgUrl),
                  ),
              ],
            ),
            const SizedBox(height: 10.0),
            Text(
              _event.name,
              style: theme.textTheme.headline5,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 5.0),
            _event.status == EventStatus.active
                ? Text('В процессе', style: TextStyle(color: theme.colorScheme.primary))
                : const Text('Завершено'),
          ],
        ),
      ),
    );
  }
}
