import 'package:flutter/material.dart';
import 'package:party_cash_controller/features/widgets/fade_in_from_network/fade_in.dart';

class EventImage extends StatelessWidget {
  final String _imageUrl;

  EventImage(this._imageUrl);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return Container(
      height: 150.0,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.vertical(
          top: Radius.zero,
          bottom: Radius.circular(8.0),
        ),
        color: theme.colorScheme.surface,
      ),
      child: ClipRRect(
        borderRadius: const BorderRadius.vertical(
          top: Radius.zero,
          bottom: Radius.circular(8.0),
        ),
        child: FadeIn(
          imgUrl: _imageUrl,
          errorBuilder: (context, exception, stacktrace) {
            return Padding(
              padding: const EdgeInsets.all(4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(Icons.warning),
                  const SizedBox(width: 5.0),
                  const Text('Не удалось загрузить изображение')
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
