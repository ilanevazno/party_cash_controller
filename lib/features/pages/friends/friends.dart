import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:party_cash_controller/features/pages/friends/widgets/friend_view/friend_view.dart';

class FriendsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) => Scaffold(
        appBar: AppBar(title: const Text('Друзья')),
        body: ListView(
          children: [
            FriendView(
              fullName: 'Просто Илон',
              tag: '@ilonvmaske',
              phone: '+79099099999',
              imgUrl:
                  'https://motor.ru/imgs/2020/11/19/10/4352507/1b4de4336e196eb2f0b03caf177f7318c3058b17.jpg',
            ),
            FriendView(
              fullName: 'Леня Барышников',
              tag: '@seniorsmoker',
              phone: '+78088088888',
              imgUrl:
                  'https://n1s1.hsmedia.ru/20/71/83/20718398d166214ff8fffe0df3db2cda/330x500_0xac120003_14304862181562658318.jpg',
            ),
            FriendView(
              fullName: 'Илюха Сигидин',
              tag: '@hochuvkachalku',
              phone: '+77077077777',
              imgUrl:
                  'https://medialeaks.ru/wp-content/uploads/2020/05/imgonline-com-ua-Resize-XeUZn1Qeis9fwWYL-600x446.jpg',
            )
          ],
        ),
      ),
    );
  }
}
