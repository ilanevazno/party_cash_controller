import 'package:flutter/material.dart';
import 'package:party_cash_controller/features/widgets/fade_in_from_network/fade_in.dart';
import 'package:party_cash_controller/models/user_model/user_model.dart';

class FriendView extends StatelessWidget {
  final String fullName;
  final String tag;
  final String phone;
  final String imgUrl;

  final List<UserModel> friends = [];

  FriendView({
    required this.fullName,
    required this.tag,
    required this.phone,
    required this.imgUrl,
  });

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return Card(
      child: ListTile(
        leading: Container(
          width: 60.0,
          height: 60.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: theme.colorScheme.onSurface,
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50.0),
            child: FadeIn(
              imgUrl: imgUrl,
              errorBuilder: (context, exception, stacktrace) {
                return const Icon(Icons.person);
              },
            ),
          ),
        ),
        title: Text(fullName),
        subtitle: Text('$tag | $phone'),
      ),
    );
  }
}
