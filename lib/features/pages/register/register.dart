import 'dart:io';

import 'package:party_cash_controller/features/widgets/photo_upload/photo_upload.dart';
import 'package:party_cash_controller/ioc.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:party_cash_controller/features/widgets/main_button/main_button.dart';
import 'package:party_cash_controller/services/routes.dart';
import 'package:party_cash_controller/stores/user_store/user_store.dart';
import 'package:party_cash_controller/utils/flushbar.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController fullnameFieldController = TextEditingController();
  // TODO: set up for ioc https://pub.dev/packages/image_picker
  final picker = ImagePicker();
  File? image;
  bool isFullnameValid = true;
  bool isPending = false;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Text(
                  'Добро пожаловать!',
                  style: theme.textTheme.headline5,
                ),
                margin: const EdgeInsets.only(top: 40.0),
              ),
              Container(
                child: TextField(
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Полное имя',
                    errorText: isFullnameValid ? null : 'Пожалуйста, заполните поле',
                  ),
                  controller: fullnameFieldController,
                ),
                width: 320.0,
                height: 50,
                margin: const EdgeInsets.only(top: 30.0),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: theme.colorScheme.surface,
                      ),
                      child: PhotoUpload(
                        selectedImage: image,
                        onPickedImage: _setSelectedPhoto,
                      ),
                      height: 200.0,
                      margin: const EdgeInsets.all(20.0),
                    ),
                  ),
                ],
              ),
              MainButton(
                title: 'Регистрация',
                onPressed: () => handleSubmit(),
                isDisabled: isPending,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _setSelectedPhoto(File filePath) {
    setState(() {
      image = filePath;
    });
  }

  Future<void> handleSubmit() async {
    if (fullnameFieldController.text == '') {
      setState(() => isFullnameValid = false);

      return;
    }

    setState(() => isPending = true);

    final UserStore userStore = ioc.get<UserStore>();

    try {
      await userStore.createUser(fullname: fullnameFieldController.text, image: image);

      Navigator.of(context).pushReplacementNamed(AppRoutes.main);
    } catch (e) {
      print(e);
      showFlushbar(
        context,
        type: FlushbarType.error,
        message: 'Что-то пошло не так.\nПожалуйста, попробуйте позже',
      );
    } finally {
      setState(() => isPending = false);
    }
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        image = File(pickedFile.path);
      } else {
        print('No image selected');
      }
    });
  }
}
