import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:after_layout/after_layout.dart';
import 'package:party_cash_controller/ioc.dart';
import 'package:party_cash_controller/services/routes.dart';
import 'package:party_cash_controller/stores/auth_store/auth_store.dart';
import 'package:party_cash_controller/stores/user_store/user_store.dart';

class StartUpPage extends StatefulWidget {
  @override
  _StartUpPageState createState() => _StartUpPageState();
}

class _StartUpPageState extends State<StartUpPage> with AfterLayoutMixin<StartUpPage> {
  bool isError = false;

  @override
  afterFirstLayout(BuildContext context) async {
    try {
      await initialize();

      final bool isAuthorized = ioc.get<AuthStore>().isUserAuthorized;
      final NavigatorState navigator = Navigator.of(context);

      if (isAuthorized) {
        final UserStore userStore = ioc.get<UserStore>();
        final bool isNewUser = await userStore.isNewUser;

        if (isNewUser) {
          navigator.pushReplacementNamed(AppRoutes.register);
        } else {
          await userStore.loadUser();

          navigator.pushReplacementNamed(AppRoutes.main);
        }
      } else {
        navigator.pushReplacementNamed(AppRoutes.auth);
      }
    } catch (e) {
      // TODO: сделать более понятную пользователю обработку ошибку,
      // настроить логирование и транспорт логов (глобально по проекту
      // здесь пишу только чтобы не забыть)
      print('Initialization error: $e');
      setState(() => isError = true);
      return;
    }
  }

  Future<void> initialize() async {
    await Firebase.initializeApp();
    configureDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 80.0),
              Text(
                'Party cash controller',
                style: theme.textTheme.headline4,
                textAlign: TextAlign.center,
              ),
              Container(
                height: 80.0,
                child: isError
                    ? Padding(
                        padding: const EdgeInsets.only(top: 30.0),
                        child: Text(
                          'Возникли проблемы.\nПожалуйста, попробуйте позже',
                          style: TextStyle(color: theme.colorScheme.error),
                          textAlign: TextAlign.center,
                        ),
                      )
                    : null,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
