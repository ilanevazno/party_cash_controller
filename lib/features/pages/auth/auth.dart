import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:mobx/mobx.dart';
import 'package:party_cash_controller/features/widgets/main_button/main_button.dart';
import 'package:party_cash_controller/services/routes.dart';
import 'package:party_cash_controller/stores/auth_store/auth_store.dart';
import 'package:party_cash_controller/ioc.dart';
import 'package:party_cash_controller/utils/app_exception.dart';
import 'package:party_cash_controller/utils/flushbar.dart';

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  final AuthStore authStore = ioc.get<AuthStore>();
  final TextEditingController phoneFieldController = TextEditingController();

  ObservableFuture<void> validatePhoneNumberFuture = ObservableFuture.value(null);
  ReactionDisposer? disposeAutorun;

  bool isPending = false;
  AppException? error;

  @override
  void dispose() {
    if (disposeAutorun != null) disposeAutorun!();
    phoneFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 25.0),
              width: 300,
              child: InternationalPhoneNumberInput(
                countries: ['RU'],
                maxLength: 13,
                isEnabled: !isPending,
                hintText: 'Номер телефона',
                textFieldController: phoneFieldController,
                onInputChanged: null,
              ),
            ),
            const SizedBox(height: 50.0),
            MainButton(
              title: 'Вход',
              isDisabled: isPending,
              onPressed: () => handleSubmit(),
            ),
          ],
        ),
      ),
    );
  }

  void handleSubmit() async {
    setState(() => isPending = true);

    try {
      await validatePhoneNumber();
    } catch (e) {
      showFlushbar(context,
          message: e is AppException
              ? getErorText(e)
              : 'Возникли пробелемы. Пожалуйста, попробуйте позже',
          type: FlushbarType.error);
    }

    setState(() => isPending = false);
  }

  Future<void> validatePhoneNumber() async {
    try {
      final phone = await PhoneNumber.getRegionInfoFromPhoneNumber(
        phoneFieldController.text,
        'RU',
      );

      if (phone.toString().length != 12) {
        throw AppException.auth('Invalid phone number');
      }

      await authStore.verifyPhoneNumber(
        phone.toString(),
        onRequestOtp: () {
          Navigator.pushReplacementNamed(context, AppRoutes.otp);
        },
        onVerificationFailed: (e) {
          throw e;
        },
      );
    } catch (e) {
      if (e is AppException) {
        return Future<void>.error(e);
      } else {
        return Future<void>.error(
          AppException.auth(
            'Invalid phone number',
          ),
        );
      }
    }
  }

  String getErorText(AppException e) {
    switch (e.message) {
      case 'Invalid phone number':
        return 'Некорректный номер';

      default:
        return 'Возникли пробелемы. Пожалуйста, попробуйте позже';
    }
  }
}
