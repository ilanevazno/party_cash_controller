import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class InputField extends StatelessWidget {
  final String placeholder;
  final TextInputType keyboardType;
  final bool obscrureText;
  final String fieldName;
  TextEditingController? controller;

  InputField({
    required this.placeholder,
    this.keyboardType = TextInputType.text,
    this.obscrureText = false,
    this.fieldName = '',
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      controller: controller,
      obscureText: obscrureText,
      keyboardType: keyboardType,
      name: fieldName,
      decoration: InputDecoration(
        border: const OutlineInputBorder(),
        contentPadding: const EdgeInsets.symmetric(horizontal: 10.0),
        hintText: placeholder,
      ),
    );
  }
}
