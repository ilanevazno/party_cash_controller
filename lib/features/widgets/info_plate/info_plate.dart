import 'package:flutter/material.dart';

enum InfoPlateType { info, warning, error }

class InfoPlate extends StatelessWidget {
  final String _message;
  final InfoPlateType _type;

  const InfoPlate(this._message, this._type);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    final Color bgColor;
    final TextStyle? textTheme = theme.accentTextTheme.subtitle1;

    switch (_type) {
      case InfoPlateType.error:
      case InfoPlateType.warning:
        bgColor = theme.accentColor;
        break;

      case InfoPlateType.info:
        bgColor = theme.hintColor;
        break;

      default:
        bgColor = theme.accentColor;
    }

    return Row(
      children: [
        Expanded(
          child: Container(
            color: bgColor,
            height: 80.0,
            child: Center(
              child: Text(_message, style: textTheme),
            ),
          ),
        ),
      ],
    );
  }
}
