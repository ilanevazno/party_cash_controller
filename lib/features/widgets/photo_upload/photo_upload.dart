import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PhotoUpload extends StatelessWidget {
  final picker = ImagePicker();
  dynamic selectedImage;
  void Function(File image) onPickedImage;

  PhotoUpload({required this.onPickedImage, this.selectedImage});

  @override
  Widget build(BuildContext context) {
    return Flexible(
        child: Container(
      width: double.infinity,
      height: 130,
      child: MaterialButton(
        onPressed: _getImageFromGallery,
        child: selectedImage != null
            ? Container(
                decoration: BoxDecoration(
                    image: DecorationImage(image: FileImage(selectedImage), fit: BoxFit.cover)),
              )
            : Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(Icons.camera_alt),
                    Container(
                      child: const Text('Загрузить фото'),
                      margin: const EdgeInsets.only(top: 5),
                    )
                  ],
                ),
              ),
      ),
    ));
  }

  Future _getImageFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      File imagePath = File(pickedFile.path);

      onPickedImage(imagePath);
    } else {
      print('No image selected');
    }
  }
}
