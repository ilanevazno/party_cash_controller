import 'package:flutter/material.dart';

enum ButtonView { primary, secondary }

class MainButton extends StatelessWidget {
  final String title;
  final void Function() onPressed;
  final ButtonView view;
  final bool isDisabled;

  MainButton({
    required this.title,
    required this.onPressed,
    this.isDisabled = false,
    this.view = ButtonView.primary,
  });

  Color getBgColor(ColorScheme colorScheme) {
    switch (view) {
      case (ButtonView.primary):
        {
          return colorScheme.primary;
        }
      case (ButtonView.secondary):
        {
          return colorScheme.secondary;
        }
    }

    return colorScheme.primary;
  }

  Color getForegroundColor(ColorScheme colorScheme) {
    switch (view) {
      case (ButtonView.primary):
        {
          return colorScheme.onPrimary;
        }
      case (ButtonView.secondary):
        {
          return colorScheme.onSecondary;
        }
    }

    return colorScheme.onPrimary;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final colorScheme = theme.colorScheme;

    return ElevatedButton(
      onPressed: !isDisabled ? onPressed : null,
      child: Text(title),
      style: ButtonStyle(
        minimumSize: MaterialStateProperty.all<Size>(const Size(240.0, 45.0)),
        backgroundColor: MaterialStateProperty.all<Color>(
          !isDisabled ? getBgColor(colorScheme) : theme.disabledColor,
        ),
        foregroundColor: MaterialStateProperty.all<Color>(getForegroundColor(colorScheme)),
        shadowColor: MaterialStateProperty.all<Color>(theme.shadowColor),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
        ),
        elevation: MaterialStateProperty.all<double>(3.0),
      ),
    );
  }
}
