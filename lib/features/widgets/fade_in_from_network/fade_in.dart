import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class FadeIn extends StatelessWidget {
  final String imgUrl;
  final Widget Function(BuildContext, Object, StackTrace?)? errorBuilder;

  FadeIn({required this.imgUrl, this.errorBuilder});

  @override
  Widget build(BuildContext context) {
    print(imgUrl);

    return FadeInImage(
      placeholder: MemoryImage(kTransparentImage),
      image: NetworkImage(imgUrl),
      fit: BoxFit.cover,
      fadeOutDuration: const Duration(milliseconds: 300),
      fadeInDuration: const Duration(milliseconds: 300),
      imageErrorBuilder: errorBuilder,
    );
  }
}
