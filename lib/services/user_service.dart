import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:party_cash_controller/utils/app_exception.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class UserService {
  final FirebaseAuth _firebaseAuth;
  final FirebaseStorage _firebaseStorage;
  final CollectionReference<Map<String, dynamic>> _usersRef;

  UserService(FirebaseFirestore firestore, this._firebaseAuth, this._firebaseStorage)
      : _usersRef = firestore.collection('users');

  User? get _currentUser {
    return _firebaseAuth.currentUser;
  }

  Future<bool> isNewUser() async {
    if (_currentUser == null) throw AppException.user('Cannot define current user');

    final userDoc = await _usersRef.doc(_currentUser!.uid).get();

    return !userDoc.exists;
  }

  Future<Map<String, dynamic>> createUser({required String fullName, File? image}) async {
    final String? phone = _currentUser?.phoneNumber;
    final String? id = _currentUser?.uid;

    if (id == null || phone == null) {
      throw AppException.auth('Cannot define current user');
    }

    String? photoUrl;

    if (image != null) {
      photoUrl = await _uploadUserPhoto(image);
    }

    final Map<String, dynamic> userData = {
      'id': id,
      'fullname': fullName,
      'phone': phone,
      'photoUrl': photoUrl,
    };

    await _usersRef.doc(id).set(userData);

    return userData;
  }

  Future<Map<String, dynamic>> loadUser() async {
    final currentUser = _currentUser;

    if (currentUser == null) {
      throw AppException.auth('Cannot define current user');
    }

    final DocumentSnapshot<Map<String, dynamic>> document =
        await _usersRef.doc(currentUser.uid).get();

    final Map<String, dynamic>? data = document.data();

    if (data == null) throw AppException.auth('Cannot find user');

    data['id'] = document.id;

    return data;
  }

  Future<String> _uploadUserPhoto(File image) async {
    final User? currentUser = _currentUser;

    if (currentUser == null) {
      throw AppException.auth('Cannot define current user');
    }

    final Reference firebaseStorageRef =
        _firebaseStorage.ref().child('userPhotos/${currentUser.uid}');

    final UploadTask uploadTask = firebaseStorageRef.putFile(image);
    final taskSnapshot = await uploadTask.whenComplete(() => null);

    return await taskSnapshot.ref.getDownloadURL();
  }

  Future<void> pushEventId(String eventId) async {
    final User? currentUser = _currentUser;
    List<String> unionArray = [eventId];

    if (currentUser != null) {
      _usersRef.doc(currentUser.uid).update({'events': FieldValue.arrayUnion(unionArray)});
    }
  }
}
