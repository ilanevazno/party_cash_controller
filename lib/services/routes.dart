import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:party_cash_controller/features/pages/auth/auth.dart';
import 'package:party_cash_controller/features/pages/creating_event/creating_event.dart';
import 'package:party_cash_controller/features/pages/event/event.dart';
import 'package:party_cash_controller/features/pages/friends/friends.dart';
import 'package:party_cash_controller/features/pages/main/main.dart';
import 'package:party_cash_controller/features/pages/otp/otp.dart';
import 'package:party_cash_controller/features/pages/register/register.dart';
import 'package:party_cash_controller/models/full_event_model/full_event_model.dart';

class AppRoutes {
  static String main = '/main';
  static String auth = '/auth';
  static String otp = '/otp';
  static String register = '/register';
  static String createEvent = '/createEvent';
  static String event = '/event';
  static String friends = '/friends';
}

class AppRouter {
  static final Map<String, Widget Function(BuildContext context)> routes = {
    AppRoutes.main: (BuildContext context) => MainPage(),
    AppRoutes.auth: (BuildContext context) => AuthPage(),
    AppRoutes.otp: (BuildContext context) => OtpPage(),
    AppRoutes.register: (BuildContext context) => RegisterPage(),
    AppRoutes.friends: (BuildContext context) => FriendsPage(),
    AppRoutes.createEvent: (BuildContext context) => CreatingEventPage(),
  };

  static Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    if (settings.name == AppRoutes.event) {
      final event = settings.arguments as FullEventModel;

      return MaterialPageRoute<EventPage>(
        builder: (context) {
          return EventPage(event);
        },
      );
    }
    assert(false, 'Need to implement ${settings.name}');
    return null;
  }
}
