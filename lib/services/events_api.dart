import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:party_cash_controller/utils/app_exception.dart';

@lazySingleton
class EventsApi {
  final CollectionReference<Map<String, dynamic>> _eventsBaseInfoRef;
  final CollectionReference<Map<String, dynamic>> _usersRef;
  final CollectionReference<Map<String, dynamic>> _eventsAdditionalInfoRef;
  final FirebaseAuth _firebaseAuth;
  final FirebaseStorage _firebaseStorage;

  EventsApi(FirebaseFirestore firestore, this._firebaseAuth, this._firebaseStorage)
      : _eventsBaseInfoRef = firestore.collection('events_base_info'),
        _eventsAdditionalInfoRef = firestore.collection('events_additional_info'),
        _usersRef = firestore.collection('users');

  User? get _currentUser {
    return _firebaseAuth.currentUser;
  }

  Future<List<dynamic>?> loadNotArchivedEvents() async {
    final User? user = _currentUser;

    if (user == null) {
      throw AppException.events('Cannot define current user');
    }

    final DocumentSnapshot<Map<String, dynamic>> userDocument = await _usersRef.doc(user.uid).get();

    if (userDocument.exists) {
      Map<String, dynamic>? userData = userDocument.data();

      if (userData == null) {
        throw AppException.events('Cannot find user events');
      }

      final userEventsIds = userData['events'];

      if (userEventsIds != null && userEventsIds.length > 0) {
        // TODO: Учесть ограничение загрузки ивентов по 10 штук
        final eventsCollection =
            (await _eventsBaseInfoRef.where(FieldPath.documentId, whereIn: userEventsIds).get())
                .docs
                .toList();

        return eventsCollection.map((document) {
          String id = document.id;
          dynamic data = document.data();

          data!['id'] = id;

          return data;
        }).toList();
      }
    }

    return null;
  }

  Future<Map<String, dynamic>> loadAdditionalEventInfo(String eventId) async {
    final DocumentSnapshot<Map<String, dynamic>> document =
        await _eventsAdditionalInfoRef.doc(eventId).get();
    final Map<String, dynamic>? data = document.data();

    if (data == null) throw AppException.events('Cannot find additional event info');

    return data;
  }

  Future<Map<String, dynamic>?> createEvent({required String name, required File? image}) async {
    String? photoUrl;

    if (image != null) {
      photoUrl = await _uploadUserPhoto(image);
    }
    // TODO: Починить создание событе (писать айди ивента в пользователский массив
    // ивентов и получать ивент только после этого)
    final document = await (await _eventsBaseInfoRef.add({
      'name': name,
      'imgUrl': photoUrl,
      'creationDate': DateTime.now(),
      '_isArchived': false,
      'status': 'active',
      'currency': 'RUB',
      'user_statuses': null,
    }))
        .get();

    String id = document.id;
    Map<String, dynamic>? data = document.data();

    // TODO: Нет особого смысла запрашивать документ - создали - если все ок просто вернули данные
    // если не ок - вылетит ошибка. Так же не забыть писать айди ивента в ивенты пользователя
    if (data == null) return null;

    data['id'] = id;

    return data;
  }

  Future<String> _uploadUserPhoto(File image) async {
    final User? currentUser = _currentUser;

    if (currentUser == null) {
      throw AppException.auth('Cannot define current user');
    }

    final Reference firebaseStorageRef =
        _firebaseStorage.ref().child('userPhotos/${currentUser.uid}');

    final UploadTask uploadTask = firebaseStorageRef.putFile(image);
    final taskSnapshot = await uploadTask.whenComplete(() => null);

    return await taskSnapshot.ref.getDownloadURL();
  }
}
