import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';
import 'package:party_cash_controller/utils/app_exception.dart';

@singleton
class AuthService {
  final FirebaseAuth _firebaseAuth;

  AuthService(this._firebaseAuth);

  User? get _currentUser {
    return _firebaseAuth.currentUser;
  }

  bool isUserAuthorized() => _currentUser != null;

  Future<void> verifyPin({required String pin, required String verificationId}) async {
    final credential = PhoneAuthProvider.credential(verificationId: verificationId, smsCode: pin);

    await _firebaseAuth.signInWithCredential(credential);
  }

  Future<void> verifyPhoneNumber({
    required String phone,
    required void Function(String verificationId) onSendCode,
    required void Function(AppException e) onVerificationFailed,
  }) async {
    await _firebaseAuth.verifyPhoneNumber(
      phoneNumber: phone,
      timeout: const Duration(seconds: 60),
      verificationCompleted: (PhoneAuthCredential credential) async {
        await _firebaseAuth.signInWithCredential(credential);
      },
      verificationFailed: (FirebaseAuthException e) {
        onVerificationFailed(AppException.auth(_getErrorMessage(e)));
      },
      codeSent: (String verificationId, dynamic resendToken) async {
        onSendCode(verificationId);
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }

  String _getErrorMessage(FirebaseAuthException e) {
    switch (e.code) {
      case 'invalid-phone-number':
        return 'Invalid phone number';

      default:
        return 'Phone verification failed';
    }
  }
}
