import 'package:flutter/material.dart';
import 'package:party_cash_controller/features/pages/start_up/start_up.dart';
import 'package:party_cash_controller/services/routes.dart';
import 'package:party_cash_controller/shared/app_theme.dart';

void main() async {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: AppRouter.routes,
      onGenerateRoute: AppRouter.onGenerateRoute,
      theme: AppTheme.lightTheme,
      home: StartUpPage(),
    );
  }
}
