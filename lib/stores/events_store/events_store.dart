import 'dart:io';

import 'package:mobx/mobx.dart';
import 'package:party_cash_controller/models/base_event_model/base_event_model.dart';
import 'package:party_cash_controller/models/full_event_model/full_event_model.dart';
import 'package:party_cash_controller/services/events_api.dart';
import 'package:injectable/injectable.dart';
import 'package:party_cash_controller/stores/auth_store/auth_store.dart';
import 'package:party_cash_controller/utils/app_exception.dart';

part 'events_store.g.dart';

@lazySingleton
class EventsStore = _EventsStore with _$EventsStore;

enum EventStoreState { initial, loading, loaded, rejected }

abstract class _EventsStore with Store {
  final EventsApi _eventsApi;

  final ObservableList<FullEventModel> _fullEventList = ObservableList<FullEventModel>();

  @observable
  ObservableList<BaseEventModel> eventList = ObservableList<BaseEventModel>();

  @observable
  ObservableFuture<void> _eventListFuture = ObservableFuture.value(null);

  _EventsStore(this._eventsApi, AuthStore authStore) {
    reaction((_) => authStore.isUserAuthorized, (bool isUserAuthorized) {
      if (!isUserAuthorized) _dispose();
    });
  }

  @computed
  bool get loading {
    return _eventListFuture.status == FutureStatus.pending;
  }

  @computed
  dynamic get error {
    return _eventListFuture.error;
  }

  @action
  Future<Map<String, dynamic>?> createEvent({required String name, required File? image}) async {
    Map<String, dynamic>? eventData = await _eventsApi.createEvent(name: name, image: image);

    if (eventData != null) {
      // eventList.add(EventModel.fromJson(eventData));

      return eventData;
    } else {
      throw AppException.events('Не удалось создать эвент');
    }
  }

  @action
  Future<FullEventModel> getFullEvent(BaseEventModel baseEvent) async {
    try {
      return _fullEventList.firstWhere((fullEvent) => baseEvent.id == fullEvent.id);
    } catch (e) {
      final Map<String, dynamic> data = await _eventsApi.loadAdditionalEventInfo(baseEvent.id);

      final FullEventModel fullEvent = FullEventModel.fromJson(data);
      fullEvent.setBaseModel(baseEvent);

      _fullEventList.add(fullEvent);
      return fullEvent;
    }
  }

  @action
  Future<void> loadNotArchivedEvents() async {
    Future<void> load() async {
      List<dynamic>? events = await _eventsApi.loadNotArchivedEvents();

      void addToList(dynamic eventData) {
        eventList.add(BaseEventModel.fromJson(eventData));
      }

      if (events != null) events.forEach(addToList);
    }

    _eventListFuture = ObservableFuture(load());

    await _eventListFuture;
  }

  @action
  void _dispose() {
    eventList.clear();
    _fullEventList.clear();
  }
}
