import 'package:mobx/mobx.dart';
import 'package:party_cash_controller/services/auth_service.dart';
import 'package:injectable/injectable.dart';
import 'package:party_cash_controller/utils/app_exception.dart';

part 'auth_store.g.dart';

@singleton
class AuthStore = _AuthStore with _$AuthStore;

abstract class _AuthStore with Store {
  final AuthService _authService;

  @observable
  bool isUserAuthorized = false;

  String? _verificationId;

  _AuthStore(this._authService) : isUserAuthorized = _authService.isUserAuthorized();

  Future<void> verifyPhoneNumber(
    String phone, {
    required void Function() onRequestOtp,
    required void Function(AppException) onVerificationFailed,
  }) async {
    await _authService.verifyPhoneNumber(
      phone: phone,
      onSendCode: (String verificationCode) {
        _verificationId = verificationCode;
        onRequestOtp();
      },
      onVerificationFailed: (AppException e) {
        onVerificationFailed(e);
      },
    );
  }

  @action
  Future<void> verifyPin(String pin) async {
    final String? verificationId = _verificationId;

    if (verificationId == null) {
      throw AppException.auth('Verification id is undefined (maybe you forgot to verify phone)');
    }

    await _authService.verifyPin(pin: pin, verificationId: verificationId);

    isUserAuthorized = true;
  }

  Future<void> signOut() async {
    await _authService.signOut();

    _dispose();
  }

  @action
  void _dispose() {
    isUserAuthorized = false;
    _verificationId = null;
  }
}
