import 'dart:async';
import 'dart:io';

import 'package:mobx/mobx.dart';
import 'package:party_cash_controller/models/user_model/user_model.dart';
import 'package:injectable/injectable.dart';
import 'package:party_cash_controller/services/user_service.dart';
import 'package:party_cash_controller/stores/auth_store/auth_store.dart';

part 'user_store.g.dart';

@lazySingleton
class UserStore = _UserStore with _$UserStore;

abstract class _UserStore with Store {
  final UserService _userService;

  // Поле должно быть заполнено перед переходом главную страницу
  // Сделать поле late не позволяет мобикс, который при записи читает поле
  // и провоцирует ошибку
  @observable
  UserModel? user;

  _UserStore(this._userService, AuthStore authStore) {
    reaction((_) => authStore.isUserAuthorized, (bool isUserAuthorized) async {
      if (!isUserAuthorized) _dispose();
    });
  }

  Future<bool> get isNewUser => _userService.isNewUser();

  @action
  Future<void> createUser({required String fullname, File? image}) async {
    final Map<String, dynamic> userData = await _userService.createUser(
      fullName: fullname,
      image: image,
    );

    user = UserModel.fromJson(userData);
  }

  @action
  Future<void> addEventToUser(String eventId) async {
    await _userService.pushEventId(eventId);
  }

  @action
  Future<void> loadUser() async {
    final Map<String, dynamic> userData = await _userService.loadUser();

    user = UserModel.fromJson(userData);
  }

  @action
  void _dispose() {
    user = null;
  }
}
