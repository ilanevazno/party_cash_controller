import 'package:mobx/mobx.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel extends _UserModel with _$UserModel {
  UserModel({
    required String id,
    required String fullname,
    required String phone,
    String? photoUrl,
  }) : super(
          id: id,
          fullname: fullname,
          phone: phone,
          photoUrl: photoUrl,
        );

  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}

abstract class _UserModel with Store {
  @observable
  String id;

  @observable
  String fullname;

  @observable
  String phone;

  @observable
  String? photoUrl;

  _UserModel({
    required this.id,
    required this.fullname,
    required this.phone,
    this.photoUrl,
  });
}
