import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';
import 'package:party_cash_controller/utils/parsers.dart';
import 'package:party_cash_controller/utils/app_exception.dart';

part 'base_event_model.g.dart';

enum EventStatus { active, finished, archived }

@JsonSerializable()
class BaseEventModel extends _BaseEventModel with _$BaseEventModel {
  BaseEventModel({
    required String id,
    required String name,
    required EventStatus status,
    required DateTime creationDate,
    String? imgUrl,
  }) : super(
          id: id,
          name: name,
          status: status,
          creationDate: creationDate,
          imgUrl: imgUrl,
        );

  factory BaseEventModel.fromJson(Map<String, dynamic> json) => _$BaseEventModelFromJson(json);
}

abstract class _BaseEventModel with Store {
  @observable
  String id;

  @observable
  String name;

  @JsonKey(fromJson: _statusStringToEnum)
  @observable
  EventStatus status;

  @JsonKey(fromJson: timestampToDateTime)
  @observable
  DateTime creationDate;

  @observable
  String? imgUrl;

  _BaseEventModel({
    required this.id,
    required this.name,
    required this.status,
    required this.creationDate,
    this.imgUrl,
  });

  static EventStatus _statusStringToEnum(String stateString) {
    switch (stateString) {
      case 'active':
        return EventStatus.active;
      case 'finished':
        return EventStatus.finished;
      case 'archived':
        return EventStatus.archived;

      default:
        throw AppException.events('Incorrent event state recieved');
    }
  }
}
