import 'package:mobx/mobx.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:party_cash_controller/models/base_event_model/base_event_model.dart';

part 'full_event_model.g.dart';

@JsonSerializable()
class FullEventModel extends _FullEventModel with _$FullEventModel {
  FullEventModel({required List<String> members}) : super(members: members);

  factory FullEventModel.fromJson(Map<String, dynamic> json) => _$FullEventModelFromJson(json);
}

abstract class _FullEventModel with Store {
  late final BaseEventModel _baseEvent;

  @computed
  String get id => _baseEvent.id;

  @computed
  String get name => _baseEvent.name;

  @computed
  EventStatus get status => _baseEvent.status;

  @computed
  DateTime get creationDate => _baseEvent.creationDate;

  @computed
  String? get imgUrl => _baseEvent.imgUrl;

  // Поле добавил только для теста модели, на деле же лучше парсить участников
  // в модели и грузить недостающие данные (фотки телефоны и тд)
  @observable
  List<String> members;

  _FullEventModel({required this.members});

  void setBaseModel(BaseEventModel baseEvent) => _baseEvent = baseEvent;
}
