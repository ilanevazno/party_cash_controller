import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';
import 'package:party_cash_controller/utils/parsers.dart';

part 'event_model.g.dart';

@JsonSerializable()
class EventModel extends _EventModel with _$EventModel {
  EventModel({
    required String id,
    required String name,
    required String status,
    required DateTime creationDate,
    String? imgUrl,
  }) : super(
          id: id,
          name: name,
          status: status,
          creationDate: creationDate,
          imgUrl: imgUrl,
        );

  factory EventModel.fromJson(Map<String, dynamic> json) => _$EventModelFromJson(json);
}

abstract class _EventModel with Store {
  @observable
  String id;

  @observable
  String name;

  @observable
  String status;

  @JsonKey(fromJson: timestampToDateTime)
  @observable
  DateTime creationDate;

  @observable
  String? imgUrl;

  _EventModel({
    required this.id,
    required this.name,
    required this.status,
    required this.creationDate,
    this.imgUrl,
  });
}
