class AppException implements Exception {
  String message;
  String? type = 'app';
  String? details;

  AppException.auth(this.message, {this.details}) : type = 'auth';
  AppException.user(this.message, {this.details}) : type = 'user';
  AppException.events(this.message, {this.details}) : type = 'user';

  @override
  String toString() {
    return "Exception ($type): $message${details != null ? '\n$details' : null}";
  }
}
