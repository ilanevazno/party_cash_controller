import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

enum FlushbarType { info, warning, error }

void showFlushbar(
  BuildContext context, {
  required String message,
  required FlushbarType type,
}) {
  final ThemeData theme = Theme.of(context);

  final Color bgColor;
  final TextStyle? textTheme = theme.accentTextTheme.subtitle1;

  switch (type) {
    case FlushbarType.error:
    case FlushbarType.warning:
      bgColor = theme.accentColor;
      break;

    case FlushbarType.info:
      bgColor = theme.hintColor;
  }

  Flushbar<dynamic>(
    messageText: Text(
      message,
      textAlign: TextAlign.center,
      style: textTheme,
    ),
    flushbarPosition: FlushbarPosition.TOP,
    backgroundColor: bgColor,
    duration: const Duration(seconds: 2),
    animationDuration: const Duration(milliseconds: 500),
  ).show(context);
}
