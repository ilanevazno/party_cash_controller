import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'ioc.config.dart';

final ioc = GetIt.instance;

@injectable
@InjectableInit()
void configureDependencies() {
  ioc.registerFactory<FirebaseFirestore>(() => FirebaseFirestore.instance);
  ioc.registerFactory<FirebaseAuth>(() => FirebaseAuth.instance);
  ioc.registerFactory<FirebaseStorage>(() => FirebaseStorage.instance);

  $initGetIt(ioc);
}
