import 'package:flutter/material.dart';

class _AppColors {
  static Color primary = const Color(0xFF01a299);
  static Color primaryVariant = const Color(0xFF80CBC4);
  static Color secondary = const Color(0xFFFDD835);
  static Color surface = Colors.grey[300]!;
  static Color hint = Colors.grey[500]!;
}

class AppTheme {
  static ThemeData get lightTheme {
    return ThemeData(
      primaryColor: _AppColors.primary,
      accentColor: _AppColors.secondary,
      indicatorColor: _AppColors.secondary,
      hintColor: _AppColors.hint,
      colorScheme: const ColorScheme.light().copyWith(
        primary: _AppColors.primary,
        primaryVariant: _AppColors.primaryVariant,
        secondary: _AppColors.secondary,
        surface: _AppColors.surface,
      ),
      fontFamily: 'Montserrat',
      dividerTheme: const DividerThemeData(thickness: 0.7),
    );
  }
}
